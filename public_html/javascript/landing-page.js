var screenOrientation;
function getOrientation() {
    screenOrientation = window.innerWidth > window.innerHeight ? "Landscape" : "Portrait";
}

var fitOmbre = {};
function getFitOmbre(){
    var picFakeScreen = $( "#picFakeScreen img" ).width();
    var container = $( "#container" ).width();
    fitOmbre['leftAndRight'] = (container /2) - (picFakeScreen/2);
    fitOmbre['middle'] = picFakeScreen;
}

var elementsHeight = {};
function getHeightElements(){
    elementsHeight['menuHeight'] = $('#menuPortrait').height();
    elementsHeight['up'] = ($(window).height() - elementsHeight.menuHeight)*1.05;
    elementsHeight['vh115'] = $(window).height() * 0.115;
    elementsHeight['vh884'] = $(window).height() * 0.884;
    elementsHeight['vh884x5per'] = elementsHeight.vh884 * 1.05;
    elementsHeight['vh92'] = $(window).height() * 0.92;
    elementsHeight['negvh445'] = $(window).height() * 0.655;
}

var width;
function getWidth(){
    width = $(window).width();
}

var heightTot;
function getHeightTot(){
    heightTot = $(window).height();
}

function menuClick(){                                     
    // Changement d'état en fonction du clique (menu et zone d'ombre)
    $( "#menuPortrait" ).unbind().click(
        function() {          
          // Menu goes down
          if($('#menuPortrait').hasClass( "click" ) === true){
                $(this).animate({
                    top: elementsHeight.vh884x5per
                }, 500);
               $("#ombrePortrait").animate({
                    top: elementsHeight.vh92
                }, 500);
                
               $('#menuPortrait').removeClass('click');
               
          }else{
              $(this).animate({
                top: elementsHeight.up 
              }, 500);
               $("#ombrePortrait").animate({
                    top: elementsHeight.up
                }, 500);
                
              $('#menuPortrait').addClass('click');
          }
         
        }
    );
}

function portraitProportion(){
    $( "body" ).css("width",width);
    $( "body" ).css("height",heightTot);
    $( "#container" ).css("height",$(window).height()*1.05);
    $( "#menuPortrait" ).css("top",elementsHeight.vh92);
    $( "#ombrePortrait" ).css("top",elementsHeight.vh92);
    
    $( "#pic187" ).css("height",'12.5%');
    $("#picFakeScreen").css('height', '55%');
    $("#paragraph").css('height', '17.5%');
}

function landscapeProportion(){
    $( "body" ).css("height",heightTot);
    $( "body" ).css("width",width);
    $( "#container" ).css("height",elementsHeight.vh884);
    $( "#menuLandscape" ).css("top",elementsHeight.vh884 * 1.025);
    $( "#ombreLandscape" ).css("top",elementsHeight.vh884);
    $( "#pic187" ).css("height",'20%');
    $("#picFakeScreen").css('height', '60%');
    $("#paragraph").css('height', '20%');
}



function mobileRendererPortrait(){
    // Initialisation des sources images ainsi que de leurs taille relatif 
    $('#pic187 img').remove();
    $('#pic187').prepend('<img src="picture/mobile_ios/187_logo@3x.png"  alt="pic187">');
    $('#picFakeScreen img').remove();
    $('#picFakeScreen').prepend('<img src="picture/mobile_ios/iPhone-6S-Plus.png"  alt="picFakeScreen">');
    
    $( ".bulletPortrait" ).css("padding-top", $('.bulletPortrait').height() / 5.15);
    $("#paragraph p").first().html('Bienvenue, notre site <br> internet fait peau neuve !');
    var fontSize = $("#paragraph").height();
    $("#paragraph p").css('font-size', fontSize/6);
    $("#paragraph p").first().css('font-size', fontSize/4);
    $("#paragraph").css('margin-bottom', '10px');
}
function otherRendererPortrait(){
         // Initialisation des sources images ainsi que de leurs taille relatif 
    $('#pic187 img').remove();
    $('#pic187').prepend('<img src="picture/mobile_ios/187_logo@3x.png"  alt="pic187">');
    $('#picFakeScreen img').remove();
    $('#picFakeScreen').prepend('<img src="picture/ipad/iPad-Air-2-Mockup.png"  alt="picFakeScreen">');
    
    $( ".bulletPortrait" ).css("padding-top", $('.bulletPortrait').height() / 10.15);
    // Initialisation des sources images ainsi que de leurs taille relatif 
    $("#paragraph p").first().html("Bienvenue, notre site internet <br> fait peau neuve !");
    var fontSize = $("#paragraph").height();
    $("#paragraph p").css('font-size', fontSize/5);
    $("#paragraph p").first().css('font-size', fontSize/4);
    $("#paragraph p").css('margin-bottom', '0');
}

function mobileRendererLandscape(){
         // Initialisation des sources images ainsi que de leurs taille relatif 
    $('#pic187 img').remove();
    $('#pic187').prepend('<img src="picture/mobile_ios/187_logo@3x.png"  alt="pic187">');
    $('#picFakeScreen img').remove();
    $('#picFakeScreen').prepend('<img src="picture/ipad/iPad-Air-2-Mockup.png"  alt="picFakeScreen">');
    
    $( ".bulletLandscape" ).css("padding-top", 0);
    $("#paragraph p").first().html("Bienvenue, notre site internet <br> fait peau neuve !");
    var fontSize = $("#paragraph").height();
    $("#paragraph p").css('font-size', fontSize/5);
    $("#paragraph p").first().css('font-size', fontSize/4);
    $("#paragraph p").css('margin-bottom', '0');
}

function iPadRendererLandscape(){
    // Initialisation des sources images ainsi que de leurs taille relatif 
    $( "#menuLandscape" ).css("top",elementsHeight.vh884x5per);
    $( "#ombreLandscape" ).css("top",elementsHeight.vh884 * 1.025);

    $('#pic187 img').remove();
    $('#pic187').prepend('<img src="picture/mobile_ios/187_logo@3x.png"  alt="pic187">');
    $('#picFakeScreen img').remove();
    $('#picFakeScreen').prepend('<img src="picture/ipad/iPad-Air-2-Mockup.png"  alt="picFakeScreen">');
    
    $( ".bulletLandscape" ).css("padding-top", 0);
    $("#paragraph p").first().html("Bienvenue, notre site internet <br> fait peau neuve !");
    var fontSize = $("#paragraph").height();
    $("#paragraph p").css('font-size', fontSize/5);
    $("#paragraph p").first().css('font-size', fontSize/4);
    $("#paragraph p").css('margin-bottom', '0');
}

function otherRendererLandscape(){
    // Initialisation des sources images ainsi que de leurs taille relatif 
    $('#pic187 img').remove();
    $('#pic187').prepend('<img src="picture/mobile_ios/187_logo@3x.png"  alt="pic187">');
    $('#picFakeScreen img').remove();
    $('#picFakeScreen').prepend('<img src="picture/ipad/iPad-Air-2-Mockup.png"  alt="picFakeScreen">');
    
    $( ".bulletLandscape" ).css("padding-top", 0);
    $("#paragraph p").first().html("Bienvenue, notre site internet <br> fait peau neuve !");
    var fontSize = $("#paragraph").height();
    $("#paragraph p").css('font-size', fontSize/5);
    $("#paragraph p").first().css('font-size', fontSize/4);
    $("#paragraph p").css('margin-bottom', '0');
}

function placeOmbre(){
    // Initialisation de la zone d'ombre
    $( "#leftOmbre" ).css("width", fitOmbre.leftAndRight);
    $( "#rightOmbre" ).css("width", fitOmbre.leftAndRight);
    $( "#middleOmbre" ).css("width", fitOmbre.middle);
}
function changeMenu(){
    if(screenOrientation === 'Portrait'){
        
         $("#menuPortrait").css('display', 'block');
         $("#ombrePortrait").css('display','block');
         $("#ombreLandscape").css('display','none');
         $("#menuLandscape").css('display','none');

        $("#coordonnees").css('height', '20%');
         $("#burger").css('display', 'block');
         var fontSize = $("#coordonnees").width();
         $(".cooParagraph").css('font-size', fontSize/39);
         $(".line").css('width', '100%');
         if(width < 641){
         }else{
         }
    }else{
         $("#menuPortrait").css('display', 'none');
         $("#ombrePortrait").css('display','none');
         $("#ombreLandscape").css('display','block');
         $("#menuLandscape").css('display','block');
         var fontSize = $("#menuLandscape").width();
         $(".cooParagraph").css('font-size', fontSize/90);
         if(heightTot>1024){
            $(".cooParagraph").css('font-size', fontSize/90);
         }
    }
}
$(window).resize(function () {
    getWidth();
    getHeightTot();
    getOrientation();
    getFitOmbre();
    getHeightElements() ;  
    if(screenOrientation === 'Portrait' && width < 641){
        portraitProportion();
        mobileRendererPortrait();
    }
    else if(screenOrientation === 'Portrait' && width >= 641){
        portraitProportion();
        otherRendererPortrait();
    }   
    else if(screenOrientation === 'Landscape' && heightTot < 641){
        landscapeProportion();
        mobileRendererLandscape();
    }
    else if(screenOrientation === 'Landscape' && heightTot < 800){
       landscapeProportion();
       iPadRendererLandscape();
    }
    else{
        landscapeProportion();
        otherRendererLandscape();
    }
    menuClick();
    placeOmbre();
    changeMenu();
});

 
$(document).ready(function () {
    $(window).trigger('resize');
});

